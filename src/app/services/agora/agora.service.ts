import { Injectable } from '@angular/core';
import AgoraRTC, { IAgoraRTCClient, ICameraVideoTrack, IMicrophoneAudioTrack } from 'agora-rtc-sdk-ng';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {authorizationField} from '../api.config';
import {AgoraUser, RTCSettings} from './agora.model';
import { map } from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';



export const APP_ID = '80c96c653faa4d9ebab0ac20bc8b864b';
const TOKEN = '00680c96c653faa4d9ebab0ac20bc8b864bIAC1CX1GDlZoZ7iugu6v9HTYXRK4HhECDZFiyKrRSojGCo4kO3kAAAAAEADGdujz7kcKYQEAAQDuRwph';
const CHANNEL_NAME = 'demo_channel_name';
const BASE_URL = 'https://api.agora.io/v1/apps/';

@Injectable({
  providedIn: 'root'
})
export class AgoraService {
  public rtc: RTCSettings = {
    client: null,
    // For the local audio and video tracks.
    localAudioTrack: null,
    localVideoTrack: null,
  };

  public options = {
    appId: APP_ID,
    channel: CHANNEL_NAME,
    // Pass a token if your project enables the App Certificate.
    token: TOKEN,
    // uid: '123456' // null
  };

  public remoteUsers: AgoraUser[] = [];       // To add remote users in list
  public updateUserInfo = new BehaviorSubject<any>(null); // to update remote users name

  public remoteContainer = null;
  public acquireData = null;
  public cloudRecordData = null;
  public mode = 'individual';
  public uid = null;
  private header = new HttpHeaders().set(
    'Authorization',
    authorizationField
  ).set(
    'Content-Type',
    'application/json;charset=utf-8'
  );

  constructor(private http: HttpClient) {}

  public createRTCClient(): void {
    this.rtc.client = AgoraRTC.createClient({mode: 'rtc', codec: 'vp8'});
    // this.rtc.client = AgoraRTC.createClient({ mode: "rtc", codec: "h264" , role: 'audience'});
  }

  // comment it if you don't want virtual camera
/*  public async switchCamera(label, localTracks) {
    let cams = await AgoraRTC.getCameras(); //  all cameras devices you can use
    let currentCam = cams.find(cam => cam.label === label);
    await localTracks.setDevice(currentCam.deviceId);
  }*/

  public async localUser(token, uuid): Promise<void> {
    const uid = await this.rtc.client.join(this.options.appId, this.options.channel,
      token, uuid);
    this.rtc.localAudioTrack = await AgoraRTC.createMicrophoneAudioTrack();
    this.rtc.localVideoTrack = await AgoraRTC.createCameraVideoTrack({
      // encoderConfig: '120p',
    });
    // comment it if you want to use your camera
    // await this.switchCamera('OBS Virtual Camera', this.rtc.localVideoTrack)
    this.rtc.localAudioTrack.play();
    this.rtc.localVideoTrack.play('local-player');

    this.acquire(uid).subscribe(
      (jsonData) => {
        this.acquireData = jsonData.resourceId;
        this.cloudRecord(uid).subscribe(data => {
          console.log('>>> RECORD ON', data);
          this.uid = uid;
          this.cloudRecordData = {
            resourceId: data.resourceId,
            sid: data.sid
          };
          console.log(this.cloudRecordData);
          this.checkRecord(this.cloudRecordData.resourceId, this.cloudRecordData.sid);
        });
      },
      (err) => console.error(err),
      () => console.log('observable complete'),
    );
    await this.rtc.client.publish([this.rtc.localAudioTrack, this.rtc.localVideoTrack]);
  }

  public checkRecord(resourceId, sid): any {
    try {
      const response = this.http.get<any>(
        `${BASE_URL}${this.options.appId}/cloud_recording/resourceid/${resourceId}/sid/${sid}/mode/${this.mode}/query`,
        {
          headers: this.header
        }
      );
      response.pipe(tap((val) => console.log('RESPONSE:', val)));
      return response.subscribe(res => console.log(res));
    } catch (e) {
      console.log(e);
    }
  }

  public stopRecord(uid): any {
    console.log('STOP RECORD', uid, this.options.channel);
    return this.http.post<any>(
      `${BASE_URL}${this.options.appId}/cloud_recording/resourceid/${this.acquireData}/sid/${this.cloudRecordData.sid}/mode/${this.mode}/stop`,
      {
        cname: this.options.channel,
        uid: uid.toString(),
        clientRequest: {
          async_stop: false
        }
      },
      {
        headers: this.header,
      }
    );
  }

  public cloudRecord(uid): any {
    return this.http.post<any>(
      `${BASE_URL}${this.options.appId}/cloud_recording/resourceid/${this.acquireData}/mode/${this.mode}/start`, {
      cname: this.options.channel,
      uid: uid.toString(),
      clientRequest : {
        token: this.options.token,
        recordingConfig: {
          /* channelType
             0: (Default) Communication profile.
             1: Live broadcast profile. */
          channelType: 0,
          /* streamTypes
             0: Subscribes to audio streams only.
             1: Subscribes to video streams only.
             2: (Default) Subscribes to both audio and video streams. */
          streamTypes: 2,
          /* decryptionMode Optional
             0: (Default) None.
             5: 128-bit AES encryption, GCM mode
             6: 256-bit AES encryption, GCM mode.*/
          // decryptionMode: 0,
          /* secret The decryption password when decryption mode is enabled. */
          // secret: ''
          /* videoStreamType (Optional) Number. The type of the video stream to subscribe to.
             0: (Default) Subscribes to the high-quality stream.
             1: Subscribes to the low-quality stream.*/
          // videoStreamType: 0
          subscribeUidGroup: 0, // 1 to 2 UIDs
          maxIdleTime: 500,
        },
        recordingFileConfig: {
          avFileType: ['hls']
        },
        // snapshotConfig,
        storageConfig: {
          vendor: 1, // Amazon S3
          region: 7, // EU_CENTRAL_1
          bucket: 'agorabucketproaction',
          accessKey: 'AKIASQDUOAXRIWG75TVY',
          secretKey: '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'
        },
/*        extensionServiceConfig: {
          extensionServices: [{
            serviceName: 'aliyun_vod_service',
            serviceParam: {
              accessKey: ''
            }
          }]
        }*/
      }
    }, {
      headers: this.header
    });
  }

  public agoraServerEvents(rtc): void {

    rtc.client.on('user-published', async (user, mediaType) => {
      console.log(user, mediaType, 'user-published');
      await rtc.client.subscribe(user, mediaType);
      console.log('subscribe success');
      if (mediaType === 'video') {
        const remoteVideoTrack = user.videoTrack;
        remoteVideoTrack.play('remote-playerlist' + user.uid);
      }
      if (mediaType === 'audio') {
        const remoteAudioTrack = user.audioTrack;
        remoteAudioTrack.play();
      }
    });

    rtc.client.on('user-unpublished', user => {
      console.log(user, 'user-unpublished');
    });

    rtc.client.on('user-joined', user => {
      const id = user.uid;
      this.remoteUsers.push({ uid: +id });
      this.updateUserInfo.next(id);
    });

    // // When token-privilege-will-expire occurs, fetch a new token from the server and call renewToken to renew the token.
    // rtc.client.on('token-privilege-will-expire', async (user) => {
    //   const token = await generateToken(user.uid, this.options.channel, 1);
    //   await rtc.client.renewToken(token);
    // });
    //
    // // When token-privilege-did-expire occurs, fetch a new token from the server and call join to rejoin the channel.
    // rtc.client.on('token-privilege-did-expire', async (user) => {
    //   console.log('Fetching the new Token');
    //   const token = await generateToken(this.uid, this.options.channel, 1);
    //   console.log('Rejoining the channel with new Token');
    //   await rtc.client.join(this.options.appId, this.options.channel, token, user.uid);
    // });
  }

  public async updateToken(token): Promise<any> {
    await this.rtc.client.renewToken(token);
  }

  public acquire(uid): any {
    return this.http.post<HttpResponse<any>>(`${BASE_URL}${this.options.appId}/cloud_recording/acquire`, {
      cname: this.options.channel,
      uid: uid.toString(),
      clientRequest : {
        region: 'EU',
        resourceExpiredHour:  24,
        scene: 0
      }
    }, {
      headers: this.header
    });
  }

  public async leaveCall(): Promise<void> {
    this.rtc.localAudioTrack.close();
    this.rtc.localVideoTrack.close();


    this.rtc.client.remoteUsers.forEach(user => {
      const playerContainer = document.getElementById('remote-playerlist' + user.uid.toString());
      const _ = playerContainer && playerContainer.remove();
    });

    this.stopRecord(this.uid).subscribe((data: (any)) => {
      console.log('>>> RECORD OFF', data);
    });

    await this.rtc.client.leave();
  }
}
