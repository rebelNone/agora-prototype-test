import {IAgoraRTCClient, ICameraVideoTrack, IMicrophoneAudioTrack} from 'agora-rtc-sdk-ng';

export interface AgoraUser {
  uid: number;
  name?: string;
}

export interface RTCSettings {
  client: IAgoraRTCClient;
  localAudioTrack: IMicrophoneAudioTrack;
  localVideoTrack: ICameraVideoTrack;
}
