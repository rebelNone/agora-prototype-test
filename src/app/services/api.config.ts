
const customerKey = 'fd65dd3f9bfc4530871bbcb9e12caf29';
const customerSecret = 'da2373ece51a4e9ea6aa9ecb889173a9';
const plainCredential = customerKey + ':' + customerSecret;

// Encode with base64
const encodedCredential = btoa(plainCredential);
export const authorizationField = 'Basic ' + encodedCredential;

export const API_HEADERS = {
  Accept: 'application/json',
  Authorization: authorizationField,
  // Connection: 'close',
  'Content-Type': 'application/json; charset=utf-8',
  // Cookie: `__cfduid=${generateToken(21)}${Math.ceil(Math.random() * 10 % 10)}`
};
