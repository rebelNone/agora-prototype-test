import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_HEADERS } from './api.config';
import { APP_ID } from './agora/agora.service';


const BASE_URL = `${'https://api.agora.io/dev/v3/project/' + APP_ID + '/rtm/vendor'}`;

const customerKey = '80c96c653faa4d9ebab0ac20bc8b864b';
const customerSecret = '97ebfaba908149c2a82dcd845165883e';
const plainCredential = customerKey + ':' + customerSecret;

// Encode with base64
const encodedCredential = btoa(plainCredential);
const authorizationField = 'Basic ' + encodedCredential;

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() {
      console.log(BASE_URL);
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log('!!!!!!!!!!!', `${BASE_URL}/${request.url}`);
    const reqWithHeaders = request.clone({
      url: `${BASE_URL}/${request.url}`,
      headers: new HttpHeaders(
        request.url.endsWith('auth')
          ? API_HEADERS
          : {
            ...API_HEADERS,
            'x-agora-uid': customerKey,
            Authorization: authorizationField,
                // ${localStorage.getItem('user_token')}`,
            // user_device: localStorage.getItem('user_device')
          })
    });

    return next.handle(reqWithHeaders);
  }
}
