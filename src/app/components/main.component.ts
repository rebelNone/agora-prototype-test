import {HttpParams} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {AgoraService} from '../services/agora/agora.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public hideButtons = true;

  constructor(public agora: AgoraService, public api: ApiService) {
  }

  public ngOnInit(): void {
    this.startBasicCall().then(() => console.log('Success'));
  }

  public async startBasicCall(): Promise<void> {
    const uid = this.generateUid();
    const rtcDetails = await this.generateTokenAndUid(uid);
    console.log('uid', uid);
    console.log('rtcDetails', rtcDetails);
    this.agora.createRTCClient();
    this.agora.agoraServerEvents(this.agora.rtc);
    await this.agora.localUser(rtcDetails.token, rtcDetails.uid);

    this.hideButtons = false;
  }


  public async generateTokenAndUid(uid): Promise<any> {
    // https://test-agora.herokuapp.com/access_token?channel=test&uid=1234
    const url = 'https://test-agora.herokuapp.com/access_token?';
    const options = { params: new HttpParams({ fromString: `channel=${this.agora.options.channel}&uid=` + uid }) };
    const data = await this.api.getRequest(url, options.params).toPromise();
    return { uid, token: data.token };
  }

  public generateUid(): number {
    const length = 5;
    return (Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)));
  }

  public async logout(): Promise<void> {
    await this.agora.leaveCall();
  }
}
