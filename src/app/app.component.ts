import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  //
  // public options = {
  //   appId: '80c96c653faa4d9ebab0ac20bc8b864b',
  //   channel: 'demo_channel_name',
  //   // Pass a token if your project enables the App Certificate.
  //   token: '00680c96c653faa4d9ebab0ac20bc8b864bIAAyOnmfuddDSHhi2UOT2gQP4LO4K+HGJ0sxPPZWDB9/+I4kO3kAAAAAEADGdujzJycJYQEAAQAnJwlh',
  //   uid: '123456'
  // };

  public hide = true;

  constructor(private router: Router) {}

  public ngOnInit(): void {}

  // should be dynamic uid for user
  public open(value): void{
    this.router.navigate([`/user/${value}`]).then(r => console.log('navigate', `/user/${value}`));
    this.hide = false;
  }
}
